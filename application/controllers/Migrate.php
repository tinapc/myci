<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Default Migration class.
 *
 * Run this on terminal. e.g: php index.php migrate
 *
 * @link http://bitbucket.org/myara/myci
 * @copyright Copyright (c) 2015, Rodrigo Borges <http://rodrigoborges.info>
 */
class Migrate extends CI_Controller 
{

    public function __construct() 
    {
        parent::__construct();

        if (!$this->input->is_cli_request()) 
        {
            show_error('This action is not allowed via web request, please use terminal');
            die();
        }
    }

    public function index() 
    {
        $this->load->library('migration');

        if ($this->migration->current() === FALSE)
        {
            show_error($this->migration->error_string());
        }
    }

}